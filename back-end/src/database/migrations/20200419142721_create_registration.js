exports.up = function (knex) {
	return knex.schema.createTable('registration', function (table) {
		table.string('id').primary();
		table.string('name').notNullable();
		table.string('email').notNullable();
		table.string('cep', 8).notNullable();
		table.string('phone').notNullable();
		table.string('street').notNullable();
		table.string('neighborhood').notNullable();
		table.string('number').notNullable();
		table.string('complement').notNullable();
		table.string('city').notNullable();
		table.string('uf', 2).notNullable();
		table.string('password').notNullable();
		table.string('date').notNullable();
		table.string('update').notNullable();
	});
};

exports.down = function (knex) {
	return knex.schema.dropTableIfExists('registration');
};