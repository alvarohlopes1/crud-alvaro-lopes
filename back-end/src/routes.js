const express = require('express');

const registrationController = require('./controllers/registrationController')

const routes = express.Router();

routes.get('/registration', registrationController.index);
routes.post('/registration', registrationController.create);
routes.get('/registration/:id', registrationController.unique);
routes.put('/registration/:id', registrationController.update);
routes.delete('/registration/:id', registrationController.delete);

module.exports = routes;