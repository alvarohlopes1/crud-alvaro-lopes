const express = require('express');
const cors = require('cors');
const routes = require('./routes');

const app = express();

app.use(cors(
	// { origin: 'http://meuapp.com.br' }
));

app.use(express.json());
app.use(routes);

/**
 * Anotações de aula _____________________________________________
 *
 * Tipos de Parâmetros:
 *
 * Query Params: Parâmetros nomeados enviados na rota após "?" (Usado em Filtros e paginação);
 * Route Params: Parâmetros utilizados para identificar recursos;
 * Request Body: Corpo da requisição, utilizado para criar ou alterar recursos.
 *
 * Frameworks Usados:
 * express: https://expressjs.com/pt-br/4x/api.html#app.get.method
 * Nodemon: Node live reload - https://www.npmjs.com/package/nodemon - npm install nodemon -D
 * Knex: SQL Builder - http://knexjs.org/#Schema-index
 *
 * Creação das migrations
 * knex migrate:make nome da migrate
 * 
 * Criar tabela no banco
 * npx knex migrate:latest
 *
 */

app.listen(3333);