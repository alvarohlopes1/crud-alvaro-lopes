const crypto = require('crypto');
const connection = require('../database/connection');
const moment = require('moment');

module.exports = {

	// lists all records by paging
	async index(request, response) {

		const { page = 0 } = request.query;
		const { qtsPage = 5 } = request.query;

		const [count] = await connection('registration').count();

		const registration = await connection('registration')
			.limit(qtsPage)
			.offset((page - 1) * qtsPage)
			.select([
				'registration.id',
				'registration.name',
				'registration.email',
				'registration.cep',
				'registration.phone',
				'registration.street',
				'registration.neighborhood',
				'registration.number',
				'registration.complement',
				'registration.city',
				'registration.uf',
				'registration.password',
				'registration.date',
				'registration.update',
			]);

		response.header('X-Total-Count', count['count(*)']);

		return response.json(registration);

	},

	// takes a single record
	async unique(request, response) {

		const { id } = request.params;

		const registration = await connection('registration')
			.where('id', id)
			.select('*');

		return response.json(registration);
	},

	// creates a new record
	async create(request, response) {
		const {
			name,
			email,
			cep,
			phone,
			street,
			neighborhood,
			number,
			complement,
			city,
			uf,
			password,
			update
		} = request.body;

		const id = crypto.randomBytes(9).toString('HEX');
		const date = moment().format("DD/MM/YYYY, h:mm:ss a");

		const register = await connection('registration').insert({
			id,
			name,
			email,
			cep,
			phone,
			street,
			neighborhood,
			number,
			complement,
			city,
			uf,
			password,
			date,
			update
		});

		return response.json({ register });
	},

	// update a record
	async update(request, response) {
		const {
		name,
			email,
			cep,
			phone,
			street,
			neighborhood,
			number,
			complement,
			city,
			uf,
			date
		} = request.body;

		const { id } = request.params;
		const update = moment().format("DD/MM/YYYY, h:mm:ss a");

		await connection('registration').where('id', id).update({
			name,
			email,
			cep,
			phone,
			street,
			neighborhood,
			number,
			complement,
			city,
			uf,
			date,
			update
		});

		return response.status(204).send();
	},

	// delete a record
	async delete(request, response) {

		const { id } = request.params;

		await connection('registration')
			.where('id', id)
			.delete();

		return response.status(204).send();
	}

};
