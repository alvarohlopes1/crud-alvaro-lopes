import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from 'src/app/shared/services/register.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  formRegister: FormGroup;

  @Input() register: any;
  @Input() type: string;

  constructor(
    protected registerService: RegisterService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    if (this.register) {
      this.formRegister = this.formBuilder.group({
        name: [this.register.name, [Validators.required]],
        email: [this.register.email, [Validators.required]],
        cep: [this.register.cep, [Validators.required]],
        phone: [this.register.phone, [Validators.required]],
        street: [this.register.street, [Validators.required]],
        neighborhood: [this.register.neighborhood, [Validators.required]],
        number: [this.register.number, [Validators.required]],
        complement: [this.register.complement, [Validators.required]],
        city: [this.register.city, [Validators.required]],
        uf: [this.register.uf, [Validators.required]],
        date: [this.register.date],
        password: [null],
        repeatPassword: [null],
      });
    } else {
      this.formRegister = this.formBuilder.group({
        name: [null, [Validators.required]],
        email: [null, [Validators.required]],
        cep: [null, [Validators.required]],
        phone: [null, [Validators.required]],
        street: [null, [Validators.required]],
        neighborhood: [null, [Validators.required]],
        number: [null, [Validators.required]],
        complement: [null, [Validators.required]],
        city: [null, [Validators.required]],
        uf: [null, [Validators.required]],
        update: [null],
        password: [null],
        repeatPassword: [null],
      });
    }
  }

  saveRegister() {
    if (this.formRegister.valid) {
      this.registerService.createRegisters(this.formRegister.value).subscribe(
        success => {
          this.router.navigate(['register-list']);
        },
        error => console.log(error)
      );
    }
  }

  updateRegister(id: string) {
    this.registerService.updateRegisters(id, this.formRegister.value).subscribe(
      success => {
        this.registerService.getRegisters();
      },
      error => console.log(error)
    );
  }

}
