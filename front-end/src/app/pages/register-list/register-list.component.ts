import { Component, OnInit, OnDestroy } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { RegisterService } from 'src/app/shared/services/register.service';
import { Subscription, Subject } from 'rxjs';
import { takeUntil, } from 'rxjs/operators';

@Component({
  selector: 'app-register-list',
  templateUrl: './register-list.component.html',
  styleUrls: ['./register-list.component.scss']
})
export class RegisterListComponent implements OnInit, OnDestroy {

  length = 100;
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];
  currentPage = 1;

  registers: any;
  registerSubscription: Subscription;
  unsubscribeAll: Subject<any>;
  loading = false;

  constructor(
    private registerService: RegisterService,
  ) {
    this.unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.loadRegisters(this.currentPage, this.pageSize);

    this.registerService.reloadRegisters()
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((config) => {
        if (config) {
          this.loadRegisters(this.currentPage, this.pageSize);
        }
      });
  }

  loadRegisters(currentPage: number, pageSize: number) {
    this.loading = true;
    this.registerSubscription = this.registerService.listRegisters(currentPage, pageSize)
      .subscribe((response) => {
        if (response) {
          this.loading = false;
          this.registers = response;
        }
      }, (error) => {
        this.loading = false;
        console.log(error);
      });
  }

  pageEvent(event: PageEvent) {
    this.currentPage = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.loadRegisters(this.currentPage, this.pageSize);
  }

  deleteRegister(id: string) {
    this.registerService.deleteRegisters(id).subscribe(
      success => this.loadRegisters(this.currentPage, this.pageSize),
      error => console.log(error)
    );
  }

  ngOnDestroy() {
    this.registerService.reloadRegisters();
    this.registerSubscription.unsubscribe();
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

}
