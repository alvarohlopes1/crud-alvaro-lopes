import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterListComponent } from './pages/register-list/register-list.component';
import { NewRegisterComponent } from './pages/new-register/new-register.component';

const routes: Routes = [
  { path: '', redirectTo: 'register-list', pathMatch: 'full' },
  { path: 'register-list', component: RegisterListComponent },
  { path: 'new-register', component: NewRegisterComponent },
  { path: '**', component: RegisterListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  //
}
