import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private readonly API_URL = environment.API;

  registers = new BehaviorSubject<boolean>(false);

  constructor(
    protected http: HttpClient,
  ) { }

  // Serviços de API

  listRegisters(page: number, qtsPage: number) {
    return this.http.get<any>(`${this.API_URL}/registration?page=${page}&qtsPage=${qtsPage}`).pipe(take(1));
  }

  deleteRegisters(id: string) {
    return this.http.delete(`${this.API_URL}/registration/${id}`).pipe(take(1));
  }

  updateRegisters(id: string, register: any) {
    return this.http.put(`${this.API_URL}/registration/${id}`, register ).pipe(take(1));
  }

  createRegisters(register: any) {
    return this.http.post(`${this.API_URL}/registration`, register).pipe(take(1));
  }

  // Serviços de chamada

  getRegisters() {
    this.registers.next(true);
  }

  reloadRegisters() {
    return this.registers.asObservable();
  }
}
