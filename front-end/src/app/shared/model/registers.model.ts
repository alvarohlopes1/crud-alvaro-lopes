export class RegisterModel {
  id?: string;
  name?: string;
  email?: string;
  site?: string;
  phone?: string;
  street?: string;
  neighborhood?: string;
  number?: number;
  complement?: string;
  city?: string;
  uf?: string;
}
